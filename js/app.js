$(document).ready(function(){
	
	//Page Components
	var $VehicleContainer = $('#VehicleContainer'),
		$ItemTemplate = $('<div class="item col-xs-12 px-3 py-3 mt-2 ">'+
							'<div class="row border">'+
								'<div class="col-xs-12">'+
									'<ul class="details"></ul>'+
								'</div>'+
							'</div>'+
						  '</div>');
	//Page Form
	var $VRegistrationForm = $('form[name="vehicle-registration-form"]');

	//computation of engine displacement
	var _PageComponents = (function(){
		var $displacementLiter	= 	$VRegistrationForm.find('input[name="ed_liter"]'),
			$displacementCC 	=	$VRegistrationForm.find('input[name="ed_cc"]'),
			$displacementCI 	=	$VRegistrationForm.find('input[name="ed_ci"]');

			$displacementLiter.on('keyup', function(){
				$liter = $(this).val();
				var cc = ($liter / 0.001),
					ci = ($liter / 0.01638711)
				$displacementCC.val(cc.toFixed(4));
				$displacementCI.val(ci.toFixed(4));
			});

			$displacementCC.on('keyup', function(){
				$cc = $(this).val();
				var liter = ($cc / 1000),
					ci = ($cc * 0.0610237)
				$displacementLiter.val(liter.toFixed(2));
				$displacementCI.val(ci).toFixed(4);
			});

			$displacementCI.on('keyup', function(){
				$ci = $(this).val();
				var liter = ($ci * 0.0163871),
					cc = ($ci * 16.3871)
				$displacementLiter.val(liter.toFixed(2));
				$displacementCC.val(cc.toFixed(4));
			});
	})();

	//on form submit
	$VRegistrationForm.on('submit', function() {
		var $form = $(this),
			$btn = $(this).find('button[type="submit"]');

			//simple validation
			$('.required-field').removeClass('input-error');

			$.each($('.required-field'), function(){
				if($(this).val() == ""){
					$(this).addClass('input-error');
				}
			});

			if($('.input-error').length > 0){
				return false;
			}

			if($btn.hasClass('disabled')){
				return;
			}

			$btn.addClass('disabled');
			//prepare data
			var formData = { 'name': $form.find('input[name="name"]').val(),
						     'ed_liter': $form.find('input[name="ed_liter"]').val(),
						     'ed_cc': $form.find('input[name="ed_cc"]').val(),
						     'ed_ci': $form.find('input[name="ed_ci"]').val(),
						     'engine_power': $form.find('input[name="engine_power"]').val(),
						     'price': $form.find('input[name="price"]').val(),
						     'location': $form.find('input[name="location"]').val(),
							};

			//submit in POST
			ajaxRequest("/exam_carmudi/api/register", formData, "POST").done(function(response){
				if(response){
					$('.empty-record-message').addClass('hidden');

					var $infoList = '';
					$infoList += '<li> #'+response.id+'</li>';
					$infoList += '<li><i class="fa fa-car"></i>'+response.name+'</li>';
					$infoList += '<li>'+response.engine_displacement_liter+'L '+response.engine_displacement_cc+'CC '+response.engine_displacement_ci+'CI</li>';
					$infoList += '<li><i class="fa fa-bolt"></i>'+response.engine_power+'</li>';
					$infoList += '<li>'+response.price+'</li>';
					$infoList += '<li><i class="fa fa-map-marker"></i>'+response.location+'</li>';

					var $ItemContainer = $ItemTemplate.clone();
					$ItemContainer.find('ul.details').html($infoList);
					$VehicleContainer.prepend($ItemContainer);
				}
				$btn.removeClass('disabled');
			});

			
			return false;
	});

	//ajax method
	var ajaxRequest = function(uri, data = "", action = "GET") {
		
		if(action == "POST") {
        	var postData = data;
        	return $.ajax({
	                  url: uri,
	                  method: 'POST',
	                  processData: false,
    				  contentType: 'application/json; charset=utf-8',
	                  data: JSON.stringify(postData)
	              	});
           
        } else {
        	
           return 	$.ajax({
	                  url: uri,
	                  method: 'GET',
	                  dataType: 'json'
	              	});
            
        }
            
		
	};

	//on load fetch records
	ajaxRequest("/exam_carmudi/api/fetchall").done(function(response){
		console.log(response);
		if(response === false){
			$('.empty-record-message').removeClass('hidden');
		}else{
			$('.empty-record-message').addClass('hidden');

			$.each(JSON.parse(response), function(k,v){
				var $infoList = '';
				$infoList += '<li> #'+v.id+'</li>';
				$infoList += '<li><i class="fa fa-car"></i>'+v.name+'</li>';
				$infoList += '<li>'+v.engine_displacement_liter+'L '+v.engine_displacement_cc+'CC '+v.engine_displacement_ci+'CI</li>';
				$infoList += '<li><i class="fa fa-bolt"></i>'+v.engine_power+'</li>';
				$infoList += '<li>'+v.price+'</li>';
				$infoList += '<li><i class="fa fa-map-marker"></i>'+v.location+'</li>';

				var $ItemContainer = $ItemTemplate.clone();
				$ItemContainer.find('ul.details').html($infoList);
				// alert($Item.html());
				$VehicleContainer.prepend($ItemContainer);
			});
		}
		
	});

});