<?php

    require_once( '../db/DBSettings.php' );
    require_once('../db/DBClass.php');
    require_once('Request.php');
    require_once('Response.php');
 
    $request = new Request($_SERVER);
    $response = new Response();

    // Load settings from class
    $settings = DatabaseSettings::getSettings();
    // Apply settings to Database Class Construct
    $db = new Database($settings['dbname'], $settings['dbusername'], $settings['dbpassword'], $settings['dbhost']);

    //check if GET request
    if ($request->isGetMethod()) {
        if ($request->hasValidEndpoint()) {
            $headers = [
                'Content-Type' => 'application/json; charset=utf-8'
            ];
            $data = $db->select('vehicles', [], false,'created DESC')->result();
            $data = (sizeof($data) > 0 ? json_encode($data) : FALSE);
            $response->send(json_encode($data), 200, $headers);

        }

    }

    //check if POST request
    if ($request->isPostMethod()) {
        if ($request->hasValidEndpoint()) {
            
          
            $VehicleData = ['name' => $request->getData()->name,
                            'engine_displacement_liter' => $request->getData()->ed_liter,
                            'engine_displacement_cc' => $request->getData()->ed_cc,
                            'engine_displacement_ci' => $request->getData()->ed_ci,
                            'engine_power' => $request->getData()->engine_power,
                            'price' => $request->getData()->price,
                            'location' => $request->getData()->location,
                            'created' => date('Y-m-d H:i:s', time())
                            ];

            $db->insert('vehicles', $VehicleData);
         
            //last inserted id
            $vehicle_id = $db->id();
            $data = $db->select('vehicles', ['id' => $vehicle_id])->row();

            $headers = [
                'Content-Type' => 'application/json; charset=utf-8'
            ];
          
            $response->send(json_encode($data), 201, $headers);
        }

    }

    $response->send('url not found ' . $request->getUri());

  

?>
