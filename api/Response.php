<?php
Class Response {

    /**
     * Send Response via printing Response Data
     * Params
     *  - $headers = array()
     *  - $message = string
     *  - $code = int default: 404 Not Found
     */
    public function send($message = "Request Not Found.", $code = 404, $headers = []) {
        if(sizeof($headers) > 0){
            foreach ($headers as $key => $value) {
                //key: value pair
                header("$key: $value");
            }
        }
        //set http response status code
        http_response_code($code);
        echo $message;
        exit;
    }
}
?>