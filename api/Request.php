<?php
class Request {
    private $req_method;
    private $req_url;
    private $json_req_post_data;
    private $endpoints = ['register', 'fetchall'];


    public function __construct($server = []){
        //get Server and execution environment
        $this->req_method = $server['REQUEST_METHOD'];    
        $this->req_url = $server['REQUEST_URI'];    
        $this->json_req_post_data = json_decode(file_get_contents('php://input'));    
    }

    /**
     * Returns Request Method
     */
    public function getMethod() {
        return $this->method;
    }

    /**
     * Returns Request URI
     */
    public function getUri() {
        return $this->req_url;
    }

    /**
     * Returns Request Post Data
     */
    public function getData() {
        return $this->json_req_post_data;
    }

    /**
     * Checking of Request Method if GET
     * return Boolean TRUE if condition is TRUE, else FALSE
     */
    public function isGetMethod() {

        if(strcasecmp($this->req_method, 'get') < 1){
            return TRUE;
        }

        return FALSE;
    }

    /**
     * Checking of Request Method if POST
     * return Boolean TRUE if condition is TRUE, Else FALSE
     */
    public function isPostMethod() {

        if(strcasecmp($this->req_method, 'post') < 1){
            return TRUE;
        }
        
        return FALSE;
    }

    /**
     * Check if endpoint being called is valid
     * Returns booelan TRUE if valid, else FALSE
     */
    public function hasValidEndpoint(){
        $urlArray = parse_url($this->req_url, PHP_URL_PATH);
     
        $segments = explode('/', $urlArray);
        $key = array_search('api', $segments);
        
        if(!isset($segments[$key + 1])){
            return FALSE;
        }
        $currentSegment = $segments[$key + 1];
       
        return in_array($currentSegment, $this->endpoints);
    }
}
?>