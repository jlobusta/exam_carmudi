<!DOCTYPE html>
<html>
<head>
	<title>Carmudi Measurement Unit Test</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link type="text/css" rel="stylesheet" href="css/app.css" />
	<link type="text/css" rel="stylesheet" href="css/font-awesome.min.css" />

</head>
<body>
	 <div class="container-fluid">
		<!-- content start-->
		<div class="row">
			<!-- left content start-->
			<div class="col-xs-12 col-md-8 col-sm-7 bg-carmudi-secondary " style="height:100vh">
				<div class="row">
					<div class="col-md-12 text-center">
						<img src="img/carmudi-logo-PH.svg" class="img-fluid" style="width: 185px;">
					</div>
					<div class="col-md-12" id="VehicleContainer" style="overflow-y: auto; max-height: 90vh">
						
						<h3 class="empty-record-message hidden text-center text-warning my-4">No records found</h3>

					</div>
				</div>
			</div>
			<!-- left content end-->

			<!-- right content start-->
			<div class="col-xs-12 col-md-4 col-sm-5 bg-carmudi-blue">
				<h3 class="text-white text-center my-3">Car Registration</h3>
				<form name="vehicle-registration-form">
					<div class="form-group">
						<label for="v-name" class="text-white">Vehicle Name</label>
						<input type="text" autocomplete="off" class="form-control required-field" id="v-name" name="name" placeholder="eg. Toyota vios" autocomplete="off">
					</div>

					<div class="form-group">
						<label for="v-name" class="text-white">Engine Displacement</label>
						<div class="row">
							<div class="col-md-4 col-sm-12 mb-1">
						     	<div class="input-group">
							        <input type="number" class="form-control required-field px-0" id="inlineFormInputGroup" step="0.01" name="ed_liter" placeholder="0.0">
							        <div class="input-group-append">
							          <div class="input-group-text">L</div>
							        </div>
							    </div>
						    </div>
						    <div class="col-md-4 col-sm-12 mb-1">
						     	<div class="input-group">
							        <input type="number" class="form-control required-field px-0" id="inlineFormInputGroup" step="0.0001" name="ed_cc" placeholder="0.0">
							        <div class="input-group-append">
							          <div class="input-group-text">CC</div>
							        </div>
							    </div>
						    </div>
						    <div class="col-md-4 col-sm-12 mb-1">
						     	<div class="input-group">
							        <input type="number" class="form-control required-field px-0" id="inlineFormInputGroup" step="0.0001" name="ed_ci" placeholder="0.0">
							        <div class="input-group-append">
							          <div class="input-group-text">CI</div>
							        </div>
							    </div>
						    </div>
						</div>
					</div>

					<div class="form-group">
						<label for="v-name" class="text-white">Engine Power</label>
						<div class="input-group">
					        <input type="text" autocomplete="off" class="form-control required-field" id="inlineFormInputGroup" name="engine_power" placeholder="Engine Horse Power">
					        <div class="input-group-append">
					          <div class="input-group-text">HP</div>
					        </div>
					    </div>
					</div>

					<div class="form-group">
						<label for="v-name" class="text-white">Price</label>
						<div class="input-group">
					        <div class="input-group-prepend">
					          <div class="input-group-text">&#8369;</div>
					        </div>
					        <input type="number" class="form-control required-field" id="inlineFormInputGroup" step="0.01" name="price" placeholder="Price">
					    </div>
					</div>

					<div class="form-group">
						<label for="v-name" class="text-white">Location</label>
						<div class="input-group">
					        <div class="input-group-prepend">
					          <div class="input-group-text"><i class="fa fa-map-marker"></i></div>
					        </div>
					        <input type="text" autocomplete="off" class="form-control required-field" id="inlineFormInputGroup" name="location" placeholder="Location">
					    </div>
					</div>

					<div class="form-group">
						<div class="input-group mb-1">
					        <button type="submit" class="btn btn-outline-warning btn-block text-white">Register</button>
					    </div>
						<div class="text-center">
					        <button type="reset" class="btn btn-secondary">Cancel</button>
						</div>
					</div>

				</form>
			</div>
			<!-- right content end -->
		</div>
		<!-- content end -->
	</div>
</body>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</html>
<script src="js/app.js"></script>

